#' get the report file path
#'
#' @param ... inherited from call_report
#' @param rel_path the relative path
#'
#' @return the file path of the required file
#' @export
#' @author Stephan Struckmann, Jörg Henke
call_report2filepath <- function(..., rel_path) {
  file_content <- call_report(..., rel_path = rel_path)
  return(util_write_tempfile(file_content, rel_path))
}
