#' return the data frame of progress information for a report
#'
#' @param edition the square control edition
#' @param ImplementationGroups for validation of the edition argument
#' @param lifecycle for validation of the edition argument
#' @param admin for validation of the edition argument
#' @param lib.loc character vector describing the location of R library trees to
#'                search through, or NULL for all known trees (see `.libPaths`).
#' @param editions internal use only
#' @param ... additional parameters
#'
#' @return the result of the implementation
#' @export
#' @author Stephan Struckmann, Jörg Henke
#'
#' @importFrom tryCatchLog tryCatchLog tryLog
#' @importFrom ini read.ini
#' @importFrom futile.logger flog.appender flog.threshold flog.warn flog.error
#'                           flog.debug appender.file ERROR DEBUG FATAL INFO
#'                           WARN TRACE
#'
call_progress <- function(
  edition = names(editions),
  ImplementationGroups = c("Generic"),
  lifecycle = as.lifecycle("stable"),
  lib.loc = NULL,
  admin = FALSE,
  editions = call_list_editions4calculation(
    ImplementationGroups = ImplementationGroups,
    lifecycle = lifecycle,
    lib.loc = lib.loc,
    admin = admin
  ), ...) {
  util_log_call()

  if (!missing(editions)) stop("editions is only for technical reasons a function argument.")

  util_init_logging()
  edition <- match.arg(edition)
  util_flog_carp("Using edition %s", edition)
  on.exit({
    futile.logger::flog.info("leaving SquareControl2::call_progress")
  })
  Package <- editions[[edition]]$Package
  old <- .libPaths(new = editions[[edition]]$LibPath)
  on.exit(.libPaths(new = old), add = TRUE)
  if (!exists("call_progress", envir = asNamespace(Package), mode = "function")) {
    flog.error("could not find call_progress in %s", Package)
  } else {
    fktn <- get("call_progress", envir = asNamespace(Package), mode = "function")
    flog.debug("found call_progress in %s", Package)
    res <- tryLog(fktn(...))
    if (inherits(res, "try-error")) {
      futile.logger::flog.error(as.character(res))
    }
    return(res)
  }
}
