# SquareControl2 1.4.2

* close session on detached function calls

# SquareControl2 1.4.1

* signature corrections

# SquareControl2 1.4.0

* added call user reports functionality to list all reports of a user

# SquareControl2 1.3.13

* added call protocol functionality to redo function calls

# SquareControl2 1.3.12

* added progress call

# SquareControl2 1.3.11

* Added a `NEWS.md` file to track changes to the package.
