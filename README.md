SquareControl2 is the interface of R libraries to manage database access, execution of R scripts and writing results back to the database. 

SquareControl2 works in combination with Square², a web application for data analysis tasks.
